﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using IOF.XML.V3;
using Newtonsoft.Json;

namespace WOC.Relay.TeamGenerator {
  class Program {
    static void Main(string[] args) {
      var teams = new List<TeamEntry>();
      var classes = new List<Class>();

      /*
      var grades = new List<GradeSetting> {
        new GradeSetting { Name = "Elites", Id = "01", Course = "1", NumberOfTeams = 16, NumberOfLegs = 4, StartNumber = 1, Pad = 2 }
      };

      var eventSettings = new EventSetting {
        Name = "QB.3",
        Out = "teams.xml",
        Grades = grades
      };

      File.WriteAllText("settings.json", JsonConvert.SerializeObject(eventSettings, Formatting.Indented));
      */

      var eventSettings = JsonConvert.DeserializeObject<EventSetting>(File.ReadAllText("settings.json"));

      foreach (var grade in eventSettings.Grades) {
        classes.Add(new Class {
          Name = grade.Name,
          ShortName = grade.Name,
          Id = new Id {
            Value = grade.Id
          },
          RaceClass = new RaceClass {
            Course = new SimpleCourse {
              Name = grade.Course
            }.SingleToArray()
          }.SingleToArray()
        });

        for (int i = 1; i <= grade.NumberOfTeams; i++) {
          var people = new List<TeamEntryPerson>();

          for (int j = 1; j <= grade.NumberOfLegs; j++) {
            people.Add(new TeamEntryPerson {
              Person = new Person { Name = new PersonName { Family = "", Given = "" } },
              ControlCard = new ControlCard { Value = "" }.SingleToArray(),
              Leg = j.ToString()
            });
          }

          var teamId = (i - 1 + grade.StartNumber).ToString();

          teams.Add(new TeamEntry {
            Id = new Id { Value = teamId },
            Name = $"{grade.Name} {teamId.PadLeft(grade.Pad, '0')}",
            Organisation = new Organisation { Name = "Club" }.SingleToArray(),
            Class = classes[0].SingleToArray(),
            TeamEntryPerson = people.ToArray()
          });
        }
      }

      var entryList = new EntryList {
        CreateTime = DateTime.Now,
        CreateTimeSpecified = true,
        Event = new Event {
          Name = eventSettings.Name,
          Class = classes.ToArray(),
        },
        TeamEntry = teams.ToArray(),
      };

      using (var file = File.Create(eventSettings.Out)) {
        new XmlSerializer(typeof(EntryList)).Serialize(file, entryList);
      }

      Console.WriteLine("Done!");
      Console.ReadLine();
    }
  }

  public static class Helper {
    public static T[] SingleToArray<T>(this T input) {
      return new List<T> { input }.ToArray();
    }
  }

  public class GradeSetting {
    public string Name { get; set; }
    public string Id { get; set; }
    public string Course { get; set; }
    public int NumberOfTeams { get; set; }
    public int NumberOfLegs { get; set; }
    public int StartNumber { get; set; }
    public int Pad { get; set; }
  }

  public class EventSetting {
    public string Name { get; set; }
    public string Out { get; set; }
    public List<GradeSetting> Grades { get; set; }
  }
}